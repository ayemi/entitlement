/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nnpcgroup.cosm.ejb.production.jv;

import com.nnpcgroup.cosm.entity.contract.ModifiedCarryContract;
import com.nnpcgroup.cosm.entity.production.jv.ModifiedCarryProduction;

/**
 *
 * @author 18359
 */
public interface ModifiedCarryProductionServices extends AlternativeFundingProductionServices<ModifiedCarryProduction, ModifiedCarryContract>{
    
}
